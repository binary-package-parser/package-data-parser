package com.vic.parser;

import com.vic.parser.exception.ParserException;
import com.vic.parser.model.PackageMetric;
import com.vic.parser.model.PackageMetricData;
import com.vic.parser.model.PackageMetricHeader;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Iterator;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;

/**
 * Тестовый класс для проверки работы парсера файла
 */
public class ParserMetricFileTest {

    @Test
    public void testParseFile() throws IOException, ParserException {
        URL url = getClass().getResource("/Exp device N10520 DT 2016-10-16 18-34-03 Pos 000000.dat");
        PackageMetric packageMetric = MetricFileParser.parseFile(URLDecoder.decode(url.getFile(),
                StandardCharsets.UTF_8.displayName()));
        PackageMetricHeader header = packageMetric.getHeader();
        Assert.assertEquals(header.getDeviceID(), 10520);
        Assert.assertEquals(header.getOnDateTime(), LocalDateTime.parse("2016-10-16T18:34:03", ISO_LOCAL_DATE_TIME)
                .atZone(ZoneId.systemDefault()).toOffsetDateTime());
        Assert.assertEquals(header.getDepartment(), "НГДУ Азнакаевскнефть");
        Assert.assertEquals(header.getField(), "0");
        Assert.assertEquals(header.getPad(), "0");
        Assert.assertEquals(header.getWell(), "305");
        Assert.assertEquals(header.getShop(), "27");
        Assert.assertEquals(header.getCrew(), "10");
        Assert.assertEquals(header.getSpu(), "0");
        Assert.assertEquals(header.getFkMax(), (float) 40.0);
        Assert.assertEquals(header.getFkTal(), (float) 2.286);
        Assert.assertEquals(header.getPCtb(), 6);
        Iterator<PackageMetricData> iterator = packageMetric.getBody().iterator();
        PackageMetricData data = iterator.next();
        Assert.assertEquals(data.getOnDateTime(), LocalDateTime.parse("2016-10-16T18:34:03", ISO_LOCAL_DATE_TIME)
                .atZone(ZoneId.systemDefault()).toOffsetDateTime());
        Assert.assertEquals(data.getPid(), 5);
        Assert.assertEquals(data.getCid(), 0);
        Assert.assertEquals(data.getValue(), (float) 0.0);
        data = iterator.next();
        Assert.assertEquals(data.getOnDateTime(), LocalDateTime.parse("2016-10-16T18:34:19", ISO_LOCAL_DATE_TIME)
                .atZone(ZoneId.systemDefault()).toOffsetDateTime());
        Assert.assertEquals(data.getPid(), 512);
        Assert.assertEquals(data.getCid(), 0);
        Assert.assertEquals(data.getValue(), (float) -0.068);
        data = iterator.next();
        Assert.assertEquals(data.getOnDateTime(), LocalDateTime.parse("2016-10-16T18:34:39", ISO_LOCAL_DATE_TIME)
                .atZone(ZoneId.systemDefault()).toOffsetDateTime());
        Assert.assertEquals(data.getPid(), 512);
        Assert.assertEquals(data.getCid(), 0);
        Assert.assertEquals(data.getValue(), (float) -0.065);
    }

}