package com.vic.parser;

import com.vic.parser.model.PackageMetricData;

/**
 * Класс реализует парсер записей тела пакета
 */
public class BodyParser {

    //Размер записи в байтах
    public static final int SIZE = 12;

    /**
     * Метод для парсинга записи тела пакета
     *
     * @param data массив байт с записью тела пакета
     * @return распаршенная запись
     */
    public static PackageMetricData parse(byte[] data) {
        PackageMetricData result = new PackageMetricData();
        ParserData parserData = new ParserData(data, 0);
        result.setOnDateTime(Util.getDateFromArray(parserData));
        result.setPid(Util.getShortFromArray(parserData));
        result.setCid(Util.getShortFromArray(parserData));
        result.setValue(Util.getFloatFromArray(parserData));
        return result;
    }

}
