package com.vic.parser;

import com.vic.parser.exception.ParserException;
import com.vic.parser.model.PackageMetric;
import com.vic.parser.model.PackageMetricData;
import com.vic.parser.model.PackageMetricHeader;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Класс реализует парсинг бинарного файла с метриками
 */
@Slf4j
@Setter
public class MetricFileParser {

    /**
     * Метод для парсинга файла
     *
     * @param fileName имя файла
     * @return расспаршенный пакет
     * @throws IOException     исключения бросается при ошибки работы с файлом
     * @throws ParserException исключения бросается при ошибки работы парсера
     */
    public static PackageMetric parseFile(String fileName) throws IOException, ParserException {
        Path metricFile = Paths.get(fileName);
        log.debug("File for processing: {}", metricFile.toAbsolutePath());
        PackageMetric packageMetric;
        try (BufferedInputStream bis = new BufferedInputStream(Files.newInputStream(metricFile))) {
            packageMetric = parseStream(bis);
        }
        return packageMetric;
    }

    /**
     * Метод для парсинга файла, переданного как стрим
     *
     * @param is входной поток из которого производится чтение файла
     * @return расспаршенный пакет
     * @throws IOException     исключения бросается при ошибки работы с файлом
     * @throws ParserException исключения бросается при ошибки работы парсера
     */
    public static PackageMetric parseStream(InputStream is) throws IOException, ParserException {
        PackageMetric packageMetric = new PackageMetric();
        PackageMetricHeader packageHeader = readHeader(is);
        log.debug("header: {}", packageHeader);
        packageMetric.setHeader(packageHeader);
        List<PackageMetricData> body = new ArrayList<>();
        PackageMetricData packageData;
        do {
            packageData = readData(is);
            if (Objects.isNull(packageData)) {
                break;
            }
            log.debug("data: {}", packageData);
            body.add(packageData);
        } while (true);
        packageMetric.setBody(body);
        log.debug("packageMetric: {}", packageMetric);
        return packageMetric;
    }

    /**
     * Метод парсит запись тела пакета
     *
     * @param is входной поток из которого производится чтения байтов записи тела пакета
     * @return распаршенная запись
     * @throws IOException     исключения бросается при ошибки чтения файла
     * @throws ParserException исключения бросается при ошибки парсинга записи
     */
    private static PackageMetricData readData(InputStream is) throws IOException, ParserException {
        byte[] buffer = new byte[BodyParser.SIZE];
        int bytesNumber = is.read(buffer, 0, BodyParser.SIZE);
        if (bytesNumber == -1) {
            return null;
        }
        if (bytesNumber != BodyParser.SIZE) {
            throw new ParserException("Invalid file format. Data size must be " + BodyParser.SIZE);
        }
        return BodyParser.parse(buffer);
    }

    /**
     * Метод парсит заголовок пакета
     *
     * @param is входной поток из которого производится чтения байтов заголовка пакета
     * @return распаршенный заголовок
     * @throws IOException     исключения бросается при ошибки чтения файла
     * @throws ParserException исключения бросается при ошибки парсинга заголовка
     */
    private static PackageMetricHeader readHeader(InputStream is) throws IOException, ParserException {
        byte[] buffer = new byte[HeaderParser.SIZE];
        int bytesNumber = is.read(buffer, 0, HeaderParser.SIZE);
        if (bytesNumber != HeaderParser.SIZE) {
            throw new ParserException("Invalid file format. Header size must be " + HeaderParser.SIZE);
        }
        return HeaderParser.parse(buffer);
    }

    /**
     * Метод для парсинга файла, переданного как массив
     *
     * @param data массив с данными
     * @return расспаршенный пакет
     * @throws IOException     исключения бросается при ошибки работы с файлом
     * @throws ParserException исключения бросается при ошибки работы парсера
     */
    public static PackageMetric parseArray(byte[] data) throws IOException, ParserException {
        return parseStream(new ByteArrayInputStream(data));
    }

}
