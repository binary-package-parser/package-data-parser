package com.vic.parser.exception;

/**
 * Класс используется при возникновении ошибки парсера
 */
public class ParserException extends Exception {

    public ParserException(String message) {
        super(message);
    }

}
