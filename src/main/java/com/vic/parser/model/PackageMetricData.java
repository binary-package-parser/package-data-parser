package com.vic.parser.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.OffsetDateTime;

/**
 * Класс для хранении записи тела пакета
 */
@Setter
@Getter
@ToString
public class PackageMetricData {

    //Дата и время точки (значения)
    private OffsetDateTime onDateTime;
    //Идентификатор точки (значения)
    private short pid;
    //Канал точки (значения) для многоканальных датчиков
    private short cid;
    //Значение
    private float value;

}
