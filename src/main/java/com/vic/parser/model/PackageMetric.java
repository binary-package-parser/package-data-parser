package com.vic.parser.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Класс для хранении всей структуры пакета
 */
@Setter
@Getter
@ToString
public class PackageMetric {

    //Заголовок
    private PackageMetricHeader header;
    //Коллекция с данными измерения
    private List<PackageMetricData> body;

}
