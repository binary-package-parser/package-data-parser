package com.vic.parser.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.OffsetDateTime;

/**
 * Класс для хранении заголовка пакета
 */
@Setter
@Getter
@ToString
public class PackageMetricHeader {

    //Серийный номер прибора
    private int deviceID;
    //Дата и время измерения
    private OffsetDateTime onDateTime;
    //Наименование предприятия / подразделения / НГДУ
    private String department;
    //Месторождение
    private String field;
    //Куст
    private String pad;
    //Скважина
    private String Well;
    //Цех
    private String Shop;
    //Бригада
    private String Crew;
    //Номер СПУ (спускоподъемной установки)
    private String Spu;
    //Максимальная нагрузка на крюк, заданная пользователем в приборе, тс
    private float fkMax;
    //Вес тали, тс
    private float fkTal;
    //Передаточное число талевого блока
    private short pCtb;

}
