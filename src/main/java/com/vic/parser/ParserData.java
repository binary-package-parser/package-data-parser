package com.vic.parser;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Класс хранит данные процесса парсинга
 */
@Getter
@Setter
@AllArgsConstructor
public class ParserData {

    //Массив байтов
    private byte[] data;
    //Позиция в массиве с которой нужно начинать парсить
    private int position;

}
