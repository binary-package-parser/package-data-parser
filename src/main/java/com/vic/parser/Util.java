package com.vic.parser;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

/**
 * Утилитный класс, содержит вспомогательные методы для парсинга байтового массива
 */
public class Util {

    /**
     * Конвертирует массив байтов в число типа long.
     * Алгоритм формирования: Байты из массива читаются в обратном порябке. Затем сохраняются в переменной
     * с помощью логической операции "и" и сдвига байта влево
     *
     * @param data     массив байтов
     * @param position позиция в массиве байтов, с которой начинается чтение
     * @param size     количество байт, которых нужно прочитать
     * @return сконвертированное число
     */
    public static long convertArrayToNumber(byte[] data, int position, int size) {
        if (size > 8) {
            throw new IllegalArgumentException("size must not greater than 8");
        }
        long result = 0;
        for (int i = position + size - 1; i >= position; i--) {
            result <<= 8;
            result |= data[i] & 0xff;
        }
        return result;
    }

    /**
     * Конвертирует массив байтов в число типа int
     *
     * @param data данные парсинга
     * @return сконвертированное число
     */
    public static int getIntFromArray(ParserData data) {
        int size = 4;
        int result = (int) Util.convertArrayToNumber(data.getData(), data.getPosition(), size);
        data.setPosition(data.getPosition() + size);
        return result;
    }

    /**
     * Конвертирует массив байтов в дату
     *
     * @param data данные парсинга
     * @return сконвертированная дата
     */
    public static OffsetDateTime getDateFromArray(ParserData data) {
        int size = 4;
        LocalDateTime dateTime = LocalDateTime.ofEpochSecond(Util.convertArrayToNumber(data.getData(),
                data.getPosition(), size), 0, ZoneOffset.UTC);
        OffsetDateTime result = dateTime.atZone(ZoneId.systemDefault()).toOffsetDateTime();
        data.setPosition(data.getPosition() + size);
        return result;
    }

    /**
     * Конвертирует массив байтов в строку
     *
     * @param data   данные парсинга
     * @param length длина строки
     * @return сконвертированная строка
     */
    public static String getStringFromArray(ParserData data, int length) {
        int size = 2;
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < length; i++) {
            long symbol = Util.convertArrayToNumber(data.getData(), data.getPosition(), size);
            if (symbol != 0) {
                result.append((char) symbol);
            }
            data.setPosition(data.getPosition() + size);
        }
        return result.toString();
    }

    /**
     * Конвертирует массив байтов в число типа float
     *
     * @param data данные парсинга
     * @return сконвертированное число
     */
    public static float getFloatFromArray(ParserData data) {
        int size = 4;
        float result = Float.intBitsToFloat((int) Util.convertArrayToNumber(data.getData(), data.getPosition(), size));
        data.setPosition(data.getPosition() + size);
        return result;
    }

    /**
     * Конвертирует массив байтов в число типа short
     *
     * @param data данные парсинга
     * @return сконвертированное число
     */
    public static short getShortFromArray(ParserData data) {
        int size = 2;
        short result = (short) Util.convertArrayToNumber(data.getData(), data.getPosition(), size);
        data.setPosition(data.getPosition() + size);
        return result;
    }

}
