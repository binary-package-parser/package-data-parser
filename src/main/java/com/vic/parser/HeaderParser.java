package com.vic.parser;

import com.vic.parser.model.PackageMetricHeader;
import lombok.extern.slf4j.Slf4j;

/**
 * Класс реализует парсер записей заголовка пакета
 */
@Slf4j
public class HeaderParser {

    //Размер записи в байтах
    public static final int SIZE = 660;

    /**
     * Метод для парсинга записи заголовка пакета
     *
     * @param header массив байт с записью заголовка пакета
     * @return распаршенная запись заголовка
     */
    public static PackageMetricHeader parse(byte[] header) {
        PackageMetricHeader result = new PackageMetricHeader();
        ParserData parserData = new ParserData(header, 0);
        result.setDeviceID(Util.getIntFromArray(parserData));
        result.setOnDateTime(Util.getDateFromArray(parserData));
        result.setDepartment(Util.getStringFromArray(parserData, 40));
        result.setField(Util.getStringFromArray(parserData, 25));
        result.setPad(Util.getStringFromArray(parserData, 25));
        result.setWell(Util.getStringFromArray(parserData, 25));
        result.setShop(Util.getStringFromArray(parserData, 25));
        result.setCrew(Util.getStringFromArray(parserData, 25));
        result.setSpu(Util.getStringFromArray(parserData, 30));
        result.setFkMax(Util.getFloatFromArray(parserData));
        result.setFkTal(Util.getFloatFromArray(parserData));
        result.setPCtb(Util.getShortFromArray(parserData));
        return result;
    }

}
